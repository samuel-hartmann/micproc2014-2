Curso de Microprocessadores 2014-2
==================================

Exemplo de uso do git para colaboração em projetos.
Sugestão: utilizar [sintaxe markdown](https://daringfireball.net/projects/markdown/basics) para arquivos de texto. Ex. README.md

Versões do arquivo
------------------

segunda versão 
terceira versão: Foi criado esqueleto do projeto com exemplo de organização de arquivo para diversos colaboradores.


Dicas:
------

Podem ser criados arquivos ou diretórios para diferentes categorias de desenvolvimento: Ex: hardware, software (src e/ou bin), documentação (doc), dados (var).

Após as atribuições de tarefas, cada aluno deve criar um branch para trabalhar. Ex: $ git branch conversorAD; git checkout conversorAD
Trabalhar em lugares distintos de um mesmo arquivo evita conflitos na hora de juntar as funcionalidades (git merge): fazer a fusão de diferentes ramos.

Ver aquivo src/programa1.md



